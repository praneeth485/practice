SheBang and Comments
#The first line of the script which starts with a # is a shebang notation which says the shell to use the type of interpretor
bash , korn , kshell . . . . . But among all bash shell is developer friendly and has almot all the feature.
Printing

From Tomorrow we have to cover from here.


Variables

Local Variables
Environment Variables
Command Substitution



Inputs

Special Variables
Prompts



Functions


Redirectors & Quotes & Exit status
"$a"   '$a'
Exit Status Codes are 256 in Total :
0 to 255
0  --> Always says the exit status is successful or completed successfully.
Standard Output Redirector  >
Standard Output Redirector without overriding >>
Standward Error &>


Conditions


Loops


Basics of SED command.


Printing in colors

# Colors using \e escape seq
# echo -e "\e[COL-CODEm MESSAGE"

# Colors are two types, Background and Foreground

## Colors       Foreground          Background
# Red               31                  41
# Green             32                  42
# Yellow            33                  43
# Blue              34                  44
# Magenta           35                  45
# Cyan              36                  46

You can pass variables from command line as well, in total you can pass a maximum of 9 variables from the command line
$1 to $9
If the EXPRESSIOn in the If is true, if will be executed if not, else or elseif will be execured as per the instruction
    We have 3 loops in IF Category :
    1) Simple IF
    2) If-Else 
    3) Else-IF 


Simple IF

if [ expression ] ; then
commands will be executed
fi
If the Expression is true execute, if not comeout.

If Else

if [ expression ] ; then
commands here will be execured
else
If else faile, the command here will be execured
fi

Else If

if [ expression ] ; then
command will be executed
elif [ exp ] ; then
command
elif [ exp3 ] ; then
command
else
command will be executed
fi
If any expression is true then it executes its associated commands and if none of the expressions are true then it is going to execute the commands which are in else block.
Expressions:
Are categorized in to three
1. Numbers
2. Strings
3. Files
Operators on numbers:
-eq , -ne , -gt, -ge, -lt, -le
[ 1 -eq 1 ] 
[ 1 -ne 1 ]
Operators on Strings:
= , == , !=
[ abc = abc ]

-z , -n 

[ -z "$var" ] -> This is true if var is not having any data
[ -n "$var" ] _> This is true if var is having any data

-z and -n are inverse proportional options
Operators on files:
Lot of operators are available and you can check them using man pages of bash
[ -f file ] -> True of file exists and file is a regular file 

[ -d xyz ]  -> True if file exists and it is a directory

### Explore the file types, There are 7 types on files in Linux.
COMMENT
ACTION=$1
if [ -z "$ACTION" ]; then
echo Argument is needed, Either start/stop
exit 1
fi
if [ "$ACTION" != "start" -o "$ACTION" != "stop" -o "$ACTION" != "restart" ]; then
echo Argument is needed, Either start/stop
exit 1
fi
START() {
echo "Starting XYZ Service"
}
STOP() {
echo "Stopping XYZ Service"
}
RESTART() {
STOP
START
}
echo "Performing Simple if statements"
if [ "$ACTION" = "start" ]; then
START
exit 0
fi
if [ "$ACTION" = "stop" ]; then
STOP
exit 0
fi
if [ "$ACTION" = "restart" ]; then
RESTART
exit 0
fi
echo Performing ELSE-IF Statement
if [ "$ACTION" = "start" ]; then
START
exit 0
elif  [ "$ACTION" = "stop" ]; then
STOP
exit 0
elif [ "$ACTION" = "restart" ]; then
RESTART
exit 0
else
echo "Argument is needed, Either start/stop"
exit 1
fi
