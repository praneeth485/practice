#!/bin/bash

FUSER=student
TOMCAT_URL="http://apachemirror.wuchna.com/tomcat/tomcat-8/v8.5.57/bin/apache-tomcat-8.5.57.tar.gz"
TOMCAT_DIR="apache-tomcat-8.5.57"
WAR_URL="https://cloudcareers-devops.s3.ap-south-1.amazonaws.com/student.war"
SQL_JAR="https://cloudcareers-devops.s3.ap-south-1.amazonaws.com/mysql-connector.jar"

ID=$(id -u)
if [ $ID -ne 0 ]; then
    echo "You need to execute this script as a root user or with SUDO"
    exit 1 
fi

LOG=/tmp/stack.log 

stat() {
    if [ $1 -eq 0 ]; then 
        echo -e "\e[32m Success \e[0m"
    else 
        echo -e "\e[31m Failure \e[0m"
        exit 1 
    fi
}

echo -n "Installing the Web Service - "
yum install httpd -y  &>> $LOG 
stat $?

echo -n "Configuring Proxy - "
echo 'ProxyPass "/student" "http://localhost:8080/student"
ProxyPassReverse "/student"  "http://localhost:8080/student" '  >  /etc/httpd/conf.d/proxy.conf 
stat $?

echo -n "Setup the deafult Index file -" 
curl -s https://cloudcareers-devops.s3.ap-south-1.amazonaws.com/index.html -o /var/www/html/index.html
stat $? 

echo -n "Starting the Apache Web Service - "
systemctl enable httpd &>> $LOG 
systemctl start httpd  &>> $LOG 
stat $? 

echo -n "Installing Java - "
yum install java -y &>> $LOG 
stat $? 

echo -n "Creating a Functional User"
id $FUSER >> $LOG 
if [ $? -eq 0 ]; then 
     echo -e "\e[33m User Already There : Skipping \e[0m"
else 
   useradd $FUSER &>> $LOG 
   stat $?
fi

echo -n "Downloading Tomcat - "
wget -q $TOMCAT_URL 
tar -xf apache-tomcat-8.5.57.tar.gz 
stat $? 

echo -n "Downloading the artifact - "
cd $TOMCAT_DIR/webapps/
wget -q $WAR_URL  
chown $FUSER:$FUSER student.war  
stat $? 

echo -n "Downloading the SQL Connector - "
cd ../lib 
wget -q $SQL_JAR 
chown $FUSER:$FUSER mysql-connector.jar
stat $? 

echo -n "Configuring the Context.xml" 
cp ../../Context.xml ../conf/context.xml 
stat $? 

echo -n "Restarting the Tomcat"
cd ../bin/
./shutdown.sh  &>> $LOG 
./startup.sh &>> $LOG 
stat $?
